const fs = require('fs');
const https = require('https');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();

app.use(express.static('/home/ubuntu/tm470-project/')); //define web root directory
app.use(bodyParser.json()); //add json parser
app.use(bodyParser.urlencoded({ extended: true })); //add urlencoded parser

var mongoose = require('mongoose'); //use mongoose to connect to MongoDB
var connection = mongoose.connection; //create connection
var db = mongoose.connect('mongodb://testdbadmin:test123@127.0.0.1:27017/test'); //define connection

connection.on('error', console.error.bind(console, 'connection error:'));
        connection.once('open', function () {
                connection.db.collection("testcarparks", function(err, collection){
                        /**CLIENT API**/
                        app.get('/api/get/all', function(req, res) {
                                console.log("/api/get/all called.");

                                collection.find({}).toArray(function(err, data){
                                        res.end(JSON.stringify(data))
                                });
                        });
                        
                        app.post('/api/cp/update', function (req, res) {
                                console.log(req.body);
                                var _id = req.body.id;
                                var n = req.body.capacity;
                                console.log('id: '+_id);
                                
                                collection.update({id: _id}, {$set : {capacity: n}})
                                collection.find({id: _id}).toArray(function(err, data){
                                        res.status = 200;
                                        res.end();
                                });
                        });
                        
                        app.get('/api/get/nearme', function(req, res) {
                                console.log("api/get/nearme called.");
                                var lat = parseFloat(req.query.lat);
	                        var lng = parseFloat(req.query.lng);
	                        console.log("Lat: "+lat+", Lng: "+lng);
	                        
	                        collection.find({ geo : { $near : [ lng, lat ], $maxDistance: 0.3 } }).limit(1000).toArray(function(err, data){
                                        res.end(JSON.stringify(data))
	                        });
                        });
                });
        });

//Define SSL config
const privateKey = fs.readFileSync('/home/ubuntu/ssl/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/home/ubuntu/ssl/cert.pem', 'utf8');
const ca = fs.readFileSync('/home/ubuntu/ssl/chain.pem', 'utf8');
const options = {
	key: privateKey,
	cert: certificate,
	ca: ca
};

//Start server on port 8443
https.createServer(options, app).listen(8443);
console.log('App started - Listening on 8443.');


